class AddBigbluebuttonServerRefToRooms < ActiveRecord::Migration[5.1]
    def self.up
        add_reference :bigbluebutton_rooms, :server, foreign_key: {to_table: :bigbluebutton_servers}
    end
  
    def self.down
      remove_column :bigbluebutton_rooms, :server_id
    end
  end
  